from django.urls import path, include
from . import views

urlpatterns = [
    #path('', views.home, name = 'student-home'),
    path('about', views.about, name = 'student-about'),
    path('register', views.register, name = 'student-registration'),
]