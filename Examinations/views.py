from django.shortcuts import render
from django.http import HttpResponse
from .models import Paper

# Create your views here.
def home(request):
    context = {
        'posts':Post.objects.all() #reads all items stored in the database and displays them to the user
    }
    return render(request, 'Examinations/home.html', context)

def about(request):
    return render(request, 'Examinations/about.html', {'title':'About Page'})
